<?php

namespace App\NYULangone\ServiceBundle\Services;


use GuzzleHttp\Client;

class ApiService {

  protected $client;

  /**
   * ApiService constructor.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  public function getYandexRobots() {
    $response = $this->client->get('https://yandex.ru/robots.txt');

    return $response->getBody();
  }
}