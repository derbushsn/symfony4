<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\NYULangone\ServiceBundle\Services\ApiService;

class DefaultController extends Controller {

  protected $api;

  /**
   * DefaultController constructor.
   */
  public function __construct(ApiService $api) {
    $this->api = $api;
  }

  public function index() {
    return $this->render('index.html.twig', [
      'body' => $this->api->getYandexRobots()
    ]);
  }
}